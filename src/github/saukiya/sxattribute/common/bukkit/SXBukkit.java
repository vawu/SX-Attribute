package github.saukiya.sxattribute.common.bukkit;

import github.saukiya.sxattribute.common.Param;
import github.saukiya.sxattribute.util.nms.NMSUtils;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;

public class SXBukkit {

    private static Map<String,BukkitItem> bukkitItemMap = new HashMap<>();


    public static void registry(String materialName,BukkitItemAttribute attribute){
        try {
            Material material = Material.valueOf(materialName);
            bukkitItemMap.put(materialName.toUpperCase(),BukkitItem.create(material,attribute));
        }catch (Exception e){

        }
    }




    public static void registry(Material material,BukkitItemAttribute attribute){
        registry(material.toString(),attribute);
    }

    public static boolean hasItem(ItemStack itemStack){
        return hasMaterial(itemStack.getType().name());
    }

    public static boolean hasMaterial(String material){
        return bukkitItemMap.containsKey(material);
    }


    public static BukkitItem getBukkitItem(ItemStack itemStack){
        return getBukkitItem(itemStack.getType().name());
    }

    public static BukkitItem getBukkitItem(String material){
        return bukkitItemMap.getOrDefault(material.toUpperCase(),null);
    }

    public static BukkitItemAttribute getBukkitItemAttribute(ItemStack itemStack){
        return getBukkitItemAttribute(itemStack.getType().name());
    }

    public static BukkitItemAttribute getBukkitItemAttribute(String material){
        return getBukkitItem(material).getAttributes().get(0);
    }


    static {
        //SWORD 剑
        registry("IRON_SWORD",  BukkitItemAttribute.instance(EquipmentSlot.HAND).setBase(BukkitItemAttributeType.ATTACK_SPEED,1.6).setBase(BukkitItemAttributeType.ATTACK_DAMAGE,6));
        registry("WOOD_SWORD", BukkitItemAttribute.instance(EquipmentSlot.HAND).setBase(BukkitItemAttributeType.ATTACK_SPEED,1.6).setBase(BukkitItemAttributeType.ATTACK_DAMAGE,4));
        registry("STONE_SWORD", BukkitItemAttribute.instance(EquipmentSlot.HAND).setBase(BukkitItemAttributeType.ATTACK_SPEED,1.6).setBase(BukkitItemAttributeType.ATTACK_DAMAGE,5));
        registry("DIAMOND_SWORD", BukkitItemAttribute.instance(EquipmentSlot.HAND).setBase(BukkitItemAttributeType.ATTACK_SPEED,1.6).setBase(BukkitItemAttributeType.ATTACK_DAMAGE,7));
        registry("GOLDEN_SWORD", BukkitItemAttribute.instance(EquipmentSlot.HAND).setBase(BukkitItemAttributeType.ATTACK_SPEED,1.6).setBase(BukkitItemAttributeType.ATTACK_DAMAGE,4));

        //斧子
        registry("IRON_AXE",  BukkitItemAttribute.instance().setBase(BukkitItemAttributeType.ATTACK_SPEED,0.9).setBase(BukkitItemAttributeType.ATTACK_DAMAGE,9));
        registry("WOOD_AXE", BukkitItemAttribute.instance().setBase(BukkitItemAttributeType.ATTACK_SPEED,0.8).setBase(BukkitItemAttributeType.ATTACK_DAMAGE,7));
        registry("STONE_AXE", BukkitItemAttribute.instance().setBase(BukkitItemAttributeType.ATTACK_SPEED,0.8).setBase(BukkitItemAttributeType.ATTACK_DAMAGE,9));
        registry("DIAMOND_AXE", BukkitItemAttribute.instance().setBase(BukkitItemAttributeType.ATTACK_SPEED,1).setBase(BukkitItemAttributeType.ATTACK_DAMAGE,9));
        registry("GOLDEN_AXE", BukkitItemAttribute.instance().setBase(BukkitItemAttributeType.ATTACK_SPEED,1).setBase(BukkitItemAttributeType.ATTACK_DAMAGE,7));
        //稿子
        registry("IRON_PICKAXE",  BukkitItemAttribute.instance().setBase(BukkitItemAttributeType.ATTACK_SPEED,1.2).setBase(BukkitItemAttributeType.ATTACK_DAMAGE,4));
        registry("WOOD_PICKAXE", BukkitItemAttribute.instance().setBase(BukkitItemAttributeType.ATTACK_SPEED,1.2).setBase(BukkitItemAttributeType.ATTACK_DAMAGE,2));
        registry("STONE_PICKAXE", BukkitItemAttribute.instance().setBase(BukkitItemAttributeType.ATTACK_SPEED,1.2).setBase(BukkitItemAttributeType.ATTACK_DAMAGE,3));
        registry("DIAMOND_PICKAXE", BukkitItemAttribute.instance().setBase(BukkitItemAttributeType.ATTACK_SPEED,1.5).setBase(BukkitItemAttributeType.ATTACK_DAMAGE,5));
        registry("GOLDEN_PICKAXE", BukkitItemAttribute.instance().setBase(BukkitItemAttributeType.ATTACK_SPEED,1.5).setBase(BukkitItemAttributeType.ATTACK_DAMAGE,2));
        //锹
        registry("IRON_SHOVEL",  BukkitItemAttribute.instance().setBase(BukkitItemAttributeType.ATTACK_SPEED,1).setBase(BukkitItemAttributeType.ATTACK_DAMAGE,4.5));
        registry("WOOD_SHOVEL", BukkitItemAttribute.instance().setBase(BukkitItemAttributeType.ATTACK_SPEED,1.2).setBase(BukkitItemAttributeType.ATTACK_DAMAGE,2.5));
        registry("STONE_SHOVEL", BukkitItemAttribute.instance().setBase(BukkitItemAttributeType.ATTACK_SPEED,1.2).setBase(BukkitItemAttributeType.ATTACK_DAMAGE,3.5));
        registry("DIAMOND_SHOVEL", BukkitItemAttribute.instance().setBase(BukkitItemAttributeType.ATTACK_SPEED,1.5).setBase(BukkitItemAttributeType.ATTACK_DAMAGE,5.5));
        registry("GOLDEN_SHOVEL", BukkitItemAttribute.instance().setBase(BukkitItemAttributeType.ATTACK_SPEED,1.5).setBase(BukkitItemAttributeType.ATTACK_DAMAGE,2.5));
        //锄
        registry("IRON_HOE",  BukkitItemAttribute.instance().setBase(BukkitItemAttributeType.ATTACK_SPEED,1).setBase(BukkitItemAttributeType.ATTACK_DAMAGE,1));
        registry("WOOD_HOE", BukkitItemAttribute.instance().setBase(BukkitItemAttributeType.ATTACK_SPEED,2).setBase(BukkitItemAttributeType.ATTACK_DAMAGE,1));
        registry("STONE_HOE", BukkitItemAttribute.instance().setBase(BukkitItemAttributeType.ATTACK_SPEED,3).setBase(BukkitItemAttributeType.ATTACK_DAMAGE,1));
        registry("DIAMOND_HOE", BukkitItemAttribute.instance().setBase(BukkitItemAttributeType.ATTACK_SPEED,4).setBase(BukkitItemAttributeType.ATTACK_DAMAGE,1));
        registry("GOLDEN_HOE", BukkitItemAttribute.instance().setBase(BukkitItemAttributeType.ATTACK_SPEED,1).setBase(BukkitItemAttributeType.ATTACK_DAMAGE,1));

        //三叉戟
        registry("TRIDENT",BukkitItemAttribute.instance().setBase(BukkitItemAttributeType.ATTACK_SPEED,1.1).setBase(BukkitItemAttributeType.ATTACK_DAMAGE,9));

        //头盔
        registry("LEATHER_HELMET",BukkitItemAttribute.instance(EquipmentSlot.HEAD).setBase(BukkitItemAttributeType.ARMOR,1));
        registry("CHAINMAIL_HELMET",BukkitItemAttribute.instance(EquipmentSlot.HEAD).setBase(BukkitItemAttributeType.ARMOR,2));
        registry("IRON_HELMET",BukkitItemAttribute.instance(EquipmentSlot.HEAD).setBase(BukkitItemAttributeType.ARMOR,2));
        registry("DIAMOND_HELMET",BukkitItemAttribute.instance(EquipmentSlot.HEAD).setBase(BukkitItemAttributeType.ARMOR,3).setBase(BukkitItemAttributeType.ARMOR_TOUGHNESS,2));
        registry("GOLDEN_HELMET",BukkitItemAttribute.instance(EquipmentSlot.HEAD).setBase(BukkitItemAttributeType.ARMOR,2));
        //防具
        registry("LEATHER_CHESTPLATE",BukkitItemAttribute.instance(EquipmentSlot.CHEST).setBase(BukkitItemAttributeType.ARMOR,3));
        registry("CHAINMAIL_CHESTPLATE",BukkitItemAttribute.instance(EquipmentSlot.CHEST).setBase(BukkitItemAttributeType.ARMOR,5));
        registry("IRON_CHESTPLATE",BukkitItemAttribute.instance(EquipmentSlot.CHEST).setBase(BukkitItemAttributeType.ARMOR,6));
        registry("DIAMOND_CHESTPLATE",BukkitItemAttribute.instance(EquipmentSlot.CHEST).setBase(BukkitItemAttributeType.ARMOR,8).setBase(BukkitItemAttributeType.ARMOR_TOUGHNESS,2));
        registry("GOLDEN_CHESTPLATE",BukkitItemAttribute.instance(EquipmentSlot.CHEST).setBase(BukkitItemAttributeType.ARMOR,5));
        //护腿
        registry("LEATHER_LEGGINGS",BukkitItemAttribute.instance(EquipmentSlot.LEGS).setBase(BukkitItemAttributeType.ARMOR,2));
        registry("CHAINMAIL_LEGGINGS",BukkitItemAttribute.instance(EquipmentSlot.LEGS).setBase(BukkitItemAttributeType.ARMOR,4));
        registry("IRON_LEGGINGS",BukkitItemAttribute.instance(EquipmentSlot.LEGS).setBase(BukkitItemAttributeType.ARMOR,5));
        registry("DIAMOND_LEGGINGS",BukkitItemAttribute.instance(EquipmentSlot.LEGS).setBase(BukkitItemAttributeType.ARMOR,6).setBase(BukkitItemAttributeType.ARMOR_TOUGHNESS,2));
        registry("GOLDEN_LEGGINGS",BukkitItemAttribute.instance(EquipmentSlot.LEGS).setBase(BukkitItemAttributeType.ARMOR,3));
        //鞋子
        registry("LEATHER_BOOTS",BukkitItemAttribute.instance(EquipmentSlot.FEET).setBase(BukkitItemAttributeType.ARMOR,1));
        registry("CHAINMAIL_BOOTS",BukkitItemAttribute.instance(EquipmentSlot.FEET).setBase(BukkitItemAttributeType.ARMOR,1));
        registry("IRON_BOOTS",BukkitItemAttribute.instance(EquipmentSlot.FEET).setBase(BukkitItemAttributeType.ARMOR,2));
        registry("DIAMOND_BOOTS",BukkitItemAttribute.instance(EquipmentSlot.FEET).setBase(BukkitItemAttributeType.ARMOR,3).setBase(BukkitItemAttributeType.ARMOR_TOUGHNESS,2));
        registry("GOLDEN_BOOTS",BukkitItemAttribute.instance(EquipmentSlot.FEET).setBase(BukkitItemAttributeType.ARMOR,3));


    }


}

package github.saukiya.sxattribute.common.bukkit;

import org.bukkit.attribute.Attribute;

public enum BukkitItemAttributeType {

    ATTACK_SPEED("攻击速度"),
    ATTACK_DAMAGE("攻击力"),
    ARMOR("盔甲"),
    ARMOR_TOUGHNESS("盔甲韧性");


    BukkitItemAttributeType(String s) {

    }


    public Attribute getAttribute(){
        switch (this){
            case ATTACK_SPEED:
                return Attribute.GENERIC_ATTACK_SPEED;
            case ATTACK_DAMAGE:
                return Attribute.GENERIC_ATTACK_DAMAGE;
            case ARMOR:
                return Attribute.GENERIC_ARMOR;
            case ARMOR_TOUGHNESS:
                return Attribute.GENERIC_ARMOR_TOUGHNESS;
        }
        return null;
    }

    public void setBase(BukkitItemAttribute attribute,Object value){
        attribute.add(this.name(),value);
    }

}

package github.saukiya.sxattribute.common.bukkit;

import github.saukiya.sxattribute.common.Param;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.inventory.EquipmentSlot;

import java.util.Map;

@AllArgsConstructor
public class BukkitItemAttribute extends Param{

    @Getter
    private EquipmentSlot slot;

    public BukkitItemAttribute() {
        this(EquipmentSlot.HAND);
    }


    public static BukkitItemAttribute instance(){
        return new BukkitItemAttribute();
    }

    public static BukkitItemAttribute instance(EquipmentSlot slot){
        return new BukkitItemAttribute(slot);
    }



    public BukkitItemAttribute setBase(BukkitItemAttributeType attributeType,Object value){
        attributeType.setBase(this,value);
        return this;
    }



}

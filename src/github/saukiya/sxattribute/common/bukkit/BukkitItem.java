package github.saukiya.sxattribute.common.bukkit;

import github.saukiya.sxattribute.data.condition.EquipmentType;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.bukkit.Material;
import org.bukkit.inventory.EquipmentSlot;

import java.util.*;
import java.util.stream.Collectors;


@AllArgsConstructor
@Data
public class BukkitItem {

    private Material material;
    private List<BukkitItemAttribute> attributes = new ArrayList<>();

    public BukkitItem(Material material) {
        this.material = material;
    }

    public List<BukkitItemAttribute> getItemAttributes(EquipmentSlot slot){
        return attributes.stream().filter(attribute -> attribute.getSlot().equals(slot)).collect(Collectors.toList());
    }

    public static BukkitItem create(Material material, BukkitItemAttribute... attributes){
        BukkitItem bukkitItem = new BukkitItem(material);
        bukkitItem.attributes.addAll(Arrays.asList(attributes));
        return bukkitItem;
    }

}

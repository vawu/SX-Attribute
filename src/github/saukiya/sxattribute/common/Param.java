package github.saukiya.sxattribute.common;

import org.bukkit.configuration.MemorySection;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Param extends HashMap<String,Object> implements ConfigurationSerializable {

    public Param add(String key, Object value){
        put(key,value);
        return this;
    }

    public static Param of(Map<String,Object> objectMap){
        Param param = Param.newInstance();
        for (Entry<String, Object> entry : objectMap.entrySet()) {
            if (entry.getValue() instanceof MemorySection){
                param.add(entry.getKey(), Param.of(((MemorySection)entry.getValue()).getValues(false)));
            }else{
                param.add(entry.getKey(),entry.getValue());
            }

        }
        return param;
    }

    public String getString(String key){
        return String.valueOf(get(key));
    }

    public int getInt(String key){
        return Integer.parseInt(getString(key));
    }

    public double getDouble(String key){
        return Double.parseDouble(getString(key));
    }

    public boolean getBoolean(String key){
        return Boolean.parseBoolean(getString(key));
    }

    public Param getParam(String key){
        return Param.of((Map<String, Object>) get(key));
    }

    public Param push(Param param){
        for (Entry<String, Object> entry : param.entrySet()) {
            add(entry.getKey(),entry.getValue());
        }
        return this;
    }

    public <E> List<E> getList(String key,Class<?> c){
        List<?> list = (List<?>) get(key);
        List<E> eList = new ArrayList<>();
        for (Object o : list) {
            eList.add((E) c.cast(o));
        }
        return eList;
    }

    public static Param newInstance(){
        return new Param();
    }

    @Override
    public Map<String, Object> serialize() {
        return this;
    }
}

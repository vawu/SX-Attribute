package github.saukiya.sxattribute.common;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class SFileUtils {


    public static List<File> listFile(File file){
        List<File> files = new ArrayList<>();
        if (file.isDirectory()){
            for (File listFile : file.listFiles()) {
                files.addAll(listFile(listFile));
            }
        }else{
            files.add(file);
        }
        return files;
    }

    public static List<ConfigurationSection> listConfigurationSection(File file){
        List<File> files = listFile(file);
        List<ConfigurationSection> configurationSections = new ArrayList<>();
        files.forEach(f -> {
            YamlConfiguration yamlConfiguration = YamlConfiguration.loadConfiguration(f);
            yamlConfiguration.getKeys(false).forEach(string -> {
                ConfigurationSection configurationSection = yamlConfiguration.getConfigurationSection(string);
                configurationSections.add(configurationSection);
            });
        });
        return configurationSections;
    }

}

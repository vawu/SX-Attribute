package github.saukiya.sxattribute.bstats;

public interface Builder<T> {
    T build();
}
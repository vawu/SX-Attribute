package github.saukiya.sxattribute.command.sub;

import github.saukiya.sxattribute.SXAttribute;
import github.saukiya.sxattribute.command.SubCommand;
import github.saukiya.sxattribute.util.nms.NMSBase;
import github.saukiya.sxattribute.util.nms.NMSUtils;
import net.minecraft.server.v1_14_R1.AttributeModifier;
import net.minecraft.server.v1_14_R1.EnumItemSlot;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_14_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * NBT显示指令
 *
 * @author Saukiya
 */
public class NBTCommand extends SubCommand {

    public NBTCommand() {
        super("nbt");
        setArg(" <ItemName>");
        setPriority(600);
    }

    @Override
    public void onCommand(CommandSender sender, String[] args) {
        ItemStack item = null;
        if (args.length > 1 && SXAttribute.getItemDataManager().hasItem(args[1])) {
            item = SXAttribute.getItemDataManager().getItem(args[1], sender instanceof Player ? (Player) sender : null);
        } else if (sender instanceof Player) {
            try {
                item = NMSUtils.getNMSBase().getMainItem((Player) sender);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            SXAttribute.getItemDataManager().sendItemMapToPlayer(sender);
            return;
        }
        String str = SXAttribute.getNMSBase().getAllNBT(item);

        try {
            sender.sendMessage("\n\n" + str + "\n");
        } catch (Exception e) {
            e.printStackTrace();
        }


    }



    @Override
    public List<String> onTabComplete(CommandSender sender, String[] args) {
        return args.length == 2 ? SXAttribute.getItemDataManager().getItemList().stream().filter(itemName -> itemName.contains(args[1])).collect(Collectors.toList()) : null;
    }
}

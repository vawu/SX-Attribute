package github.saukiya.sxattribute.data.itemdata.sub;

import github.saukiya.sxattribute.SXAttribute;
import github.saukiya.sxattribute.data.itemdata.IGenerator;
import io.lumine.xikage.mythicmobs.MythicMobs;
import io.lumine.xikage.mythicmobs.adapters.bukkit.BukkitAdapter;
import io.lumine.xikage.mythicmobs.commands.CommandHelper;
import io.lumine.xikage.mythicmobs.items.MythicItem;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Optional;

public class GeneratorMythic implements IGenerator {


    String pathName;

    String key;

    ConfigurationSection config;

    String item;

    @Override
    public JavaPlugin getPlugin() {
        return SXAttribute.getInst();
    }

    @Override
    public String getType() {
        return "mythic";
    }

    public GeneratorMythic() {
    }

    public GeneratorMythic(String pathName, String key, ConfigurationSection config) {
        this.pathName = pathName;
        this.key = key;
        this.config = config;
        this.item = config.getString("Item");
    }

    @Override
    public IGenerator newGenerator(String pathName, String key, ConfigurationSection config) {
        return new GeneratorMythic(pathName, key, config);
    }

    @Override
    public String getPathName() {
        return pathName;
    }

    @Override
    public String getKey() {
        return "mythic";
    }

    @Override
    public String getName() {
        return getKey();
    }

    @Override
    public ConfigurationSection getConfig() {
        return config;
    }

    @Override
    public ItemStack getItem(Player player) {
        ItemStack itemStack = null;
        Optional<MythicItem> maybeItem = MythicMobs.inst().getItemManager().getItem(item);
        if (maybeItem.isPresent()) {
            MythicItem mi = maybeItem.get();
            itemStack = BukkitAdapter.adapt(mi.generateItemStack(1, BukkitAdapter.adapt(player), BukkitAdapter.adapt(player)));
        }
        return itemStack;
    }

    @Override
    public ConfigurationSection saveItem(ItemStack saveItem, ConfigurationSection config) {
        return null;
    }
}

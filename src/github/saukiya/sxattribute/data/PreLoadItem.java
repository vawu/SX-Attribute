package github.saukiya.sxattribute.data;

import github.saukiya.sxattribute.SXAttribute;
import github.saukiya.sxattribute.data.condition.EquipmentType;
import github.saukiya.sxattribute.data.itemdata.IGenerator;
import github.saukiya.sxattribute.data.itemdata.ItemDataManager;
import github.saukiya.sxattribute.util.nms.NMSUtils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.bukkit.inventory.ItemStack;

/**
 * 预加载的物品
 *
 * @author Saukiya
 */

@Getter
@ToString
public class PreLoadItem {

    private EquipmentType type;

    @Setter
    private ItemStack item;

    private String inv = "DEFAULT";

    private int slot;

    public PreLoadItem(ItemStack item) {
        this.type = EquipmentType.ALL;
        this.item = item;
    }

    public PreLoadItem(EquipmentType type, ItemStack item, int slot) {
        this.type = type;
        this.item = item;
        this.slot = slot;
    }

    public PreLoadItem(EquipmentType type, ItemStack item, String inv, int slot) {
        this.type = type;
        this.item = item;
        this.inv = inv;
        this.slot = slot;
    }

    public IGenerator getGenerator(){
        String item = getItemKey();
        if (item != null){
            return SXAttribute.getItemDataManager().getItemMap().getOrDefault(item,null);
        }
        return null;
    }

    public String getItemKey(){
        String key = SXAttribute.getInst().getName()+"-Name";
        try {
            if (NMSUtils.getNMSBase().hasNBT(item,key)) {
                return NMSUtils.getNMSBase().getNBT(item, key);
            }
        } catch (Exception e) {
        }
        return null;
    }


}

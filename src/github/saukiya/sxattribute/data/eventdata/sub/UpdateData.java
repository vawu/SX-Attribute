package github.saukiya.sxattribute.data.eventdata.sub;

import github.saukiya.sxattribute.data.attribute.SXAttributeData;
import github.saukiya.sxattribute.data.eventdata.EventData;
import org.bukkit.entity.LivingEntity;

/**
 * 更新事件
 *
 * @author Saukiya
 */
public class UpdateData implements EventData {

    private final LivingEntity entity;
    private SXAttributeData sxAttributeData;

    public UpdateData(LivingEntity entity, SXAttributeData sxAttributeData) {
        this.entity = entity;
        this.sxAttributeData = sxAttributeData;
    }

    /**
     * 获取该事件的实体
     *
     * @return LivingEntity
     */
    public LivingEntity getEntity() {
        return entity;
    }

    public SXAttributeData getSxAttributeData() {
        return sxAttributeData;
    }
    @Override
    public String name() {
        return getClass().getSimpleName();
    }
}

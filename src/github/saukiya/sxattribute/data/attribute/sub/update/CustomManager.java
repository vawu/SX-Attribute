package github.saukiya.sxattribute.data.attribute.sub.update;

import github.saukiya.sxattribute.SXAttribute;
import github.saukiya.sxattribute.data.attribute.AttributeType;
import github.saukiya.sxattribute.data.attribute.SXAttributeData;
import github.saukiya.sxattribute.data.attribute.SubAttribute;
import github.saukiya.sxattribute.data.eventdata.EventData;
import github.saukiya.sxattribute.data.eventdata.sub.UpdateData;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CustomManager extends SubAttribute {

    private List<CustomAttribute> customAttributes = new ArrayList<>();

    public CustomManager() {
        super(SXAttribute.getInst(), 0,AttributeType.OTHER);
    }

    @Override
    protected YamlConfiguration defaultConfig(YamlConfiguration config) {
        config.set("power.DiscernName", "力量");
        config.set("power.Placeholder", "power");
        config.set("power.Attributes", Arrays.asList("攻击力: 1 - 2" ,"暴击伤害: +2%"));
        return config;
    }

    @Override
    public void onLoad() {
        loadConfig();
        int size = 100;
        for (String key : config().getKeys(false)) {
            ConfigurationSection configurationSection = config().getConfigurationSection(key);
            CustomAttribute customAttribute = new CustomAttribute(getPlugin(),key,configurationSection);
            customAttribute.setPriority(size);
            if (customAttribute.registerAttribute()) {
                customAttributes.add(customAttribute);
            }
            customAttributes.add(customAttribute);
            size++;
        }
    }

    @Override
    public void eventMethod(double[] values, EventData eventData) {

    }

    @Override
    public Object getPlaceholder(double[] values, Player player, String string) {
        return string.equalsIgnoreCase("size") ? String.valueOf(customAttributes.size()) : null;
    }

    @Override
    public List<String> getPlaceholders() {
        return Arrays.asList("size");
    }

    @Override
    public void loadAttributes(SXAttributeData sxAttributeData,double[] values, List<String> list) {

    }

    @Override
    public void loadAttribute(double[] values, String lore) {

    }

    public void clearAttributes(){

    }
}

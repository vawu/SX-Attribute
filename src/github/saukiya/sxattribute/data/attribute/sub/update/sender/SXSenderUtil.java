package github.saukiya.sxattribute.data.attribute.sub.update.sender;

import github.saukiya.sxattribute.util.SXUtil;

import java.lang.reflect.Constructor;

public class SXSenderUtil {



    public static Object getSXSender(){
        Object o = null;
        try {
            Class<?> clazz = Class.forName("org.bukkit.craftbukkit."+SXUtil.getVersion()+".command.CraftConsoleCommandSender");
            Constructor<?> declaredConstructor = clazz.getDeclaredConstructors()[0];
            declaredConstructor.setAccessible(true);
            o = declaredConstructor.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return o;
    }

}

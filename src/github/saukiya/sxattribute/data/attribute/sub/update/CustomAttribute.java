package github.saukiya.sxattribute.data.attribute.sub.update;

import github.saukiya.sxattribute.SXAttribute;
import github.saukiya.sxattribute.data.attribute.AttributeType;
import github.saukiya.sxattribute.data.attribute.SXAttributeData;
import github.saukiya.sxattribute.data.attribute.SubAttribute;
import github.saukiya.sxattribute.data.eventdata.EventData;
import github.saukiya.sxattribute.data.eventdata.sub.UpdateData;
import lombok.Getter;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CustomAttribute extends SubAttribute {

    private String name;
    private ConfigurationSection customConfig;
    private String placeholder;
    private List<String> attributes;
    @Getter
    private String discernName;

    public CustomAttribute(JavaPlugin plugin, String name,ConfigurationSection configurationSection) {
        super(plugin,1,AttributeType.UPDATE);
        this.name = name;
        this.customConfig = configurationSection;
        this.discernName = configurationSection.getString("DiscernName");
        this.placeholder = configurationSection.getString("Placeholder");
        this.attributes = configurationSection.getStringList("Attributes");
    }

    @Override
    public void eventMethod(double[] values, EventData eventData) {
        if (eventData instanceof UpdateData){
            SXAttributeData sxAttributeData = ((UpdateData) eventData).getSxAttributeData();
            List<String> list = new ArrayList<>();
            int value = (int) values[0];
            for (int i = 0; i < value; i++) {
                list.addAll(attributes);
            }
            sxAttributeData.add(SXAttribute.getAttributeManager().loadListData(list));
        }
    }

    @Override
    public Object getPlaceholder(double[] values, Player player, String string) {
        return string.equalsIgnoreCase(placeholder) ? values[0] : null;
    }

    @Override
    public String getName() {
        return name;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    @Override
    public List<String> getPlaceholders() {
        return Arrays.asList(placeholder);
    }

    @Override
    public void loadAttribute(double[] values, String lore) {
        if (lore.contains(discernName)){
            if (lore.startsWith("-")){
                values[0]-=getNumber(lore.replace("-",""));
            }else{
                values[0]+=getNumber(lore.replace("+",""));
            }
        }
    }
}

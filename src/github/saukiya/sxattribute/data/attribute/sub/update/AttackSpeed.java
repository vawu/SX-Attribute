package github.saukiya.sxattribute.data.attribute.sub.update;

import github.saukiya.sxattribute.SXAttribute;
import github.saukiya.sxattribute.data.PreLoadItem;
import github.saukiya.sxattribute.data.attribute.AttributeType;
import github.saukiya.sxattribute.data.attribute.SXAttributeData;
import github.saukiya.sxattribute.data.attribute.SubAttribute;
import github.saukiya.sxattribute.data.condition.EquipmentType;
import github.saukiya.sxattribute.data.eventdata.EventData;
import github.saukiya.sxattribute.data.eventdata.sub.UpdateData;
import github.saukiya.sxattribute.data.itemdata.IGenerator;
import github.saukiya.sxattribute.event.SXItemSpawnEvent;
import github.saukiya.sxattribute.event.SXItemUpdateEvent;
import github.saukiya.sxattribute.event.SXLoadAttributeEvent;
import github.saukiya.sxattribute.util.nms.NMSBase;
import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.w3c.dom.Attr;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class AttackSpeed extends SubAttribute implements Listener {


    private NMSBase NMSBase = null;

    /**
     * double[0] 攻击速度
     */
    public AttackSpeed() {
        super(SXAttribute.getInst(), 2, AttributeType.UPDATE);
    }

    @Override
    protected YamlConfiguration defaultConfig(YamlConfiguration config) {
        config.set("AttackSpeed.DiscernName", "攻击速度");
        config.set("AttackSpeed.CombatPower", 1);
        config.set("SupportIGList", Arrays.asList("SX"));
        return config;
    }

    @Override
    public void onEnable() {
        NMSBase = SXAttribute.getNMSBase();
    }

    @Override
    public void eventMethod(double[] values, EventData eventData) {
        if (eventData instanceof UpdateData && ((UpdateData) eventData).getEntity() instanceof Player) {
            Player player = (Player) ((UpdateData) eventData).getEntity();
            if (SXAttribute.getVersionSplit()[1] > 8) {
                AttributeInstance attribute = player.getAttribute(Attribute.GENERIC_ATTACK_SPEED);
                double v = (values[0] / 100 * attribute.getDefaultValue());
                double result = attribute.getDefaultValue() + (v / 2.5);
                attribute.setBaseValue(result);
            } else {
                player.setWalkSpeed((float) (values[0] / 500.0D));
            }
        }
    }

    @Override
    public Object getPlaceholder(double[] values, Player player, String string) {

        return getPlaceholders().get(0).equalsIgnoreCase(string) ? values[0] : null;
    }

    @Override
    public List<String> getPlaceholders() {
        return Arrays.asList(getName(),getName()+"Base");
    }

    @Override
    public void loadAttribute(double[] values, String lore) {
        if (lore.contains(getString("AttackSpeed.DiscernName"))) {
            if (lore.contains("%")){
                values[0] += getNumber(lore);
            }
        }
    }

    @Override
    public void correct(double[] values) {
        values[0] = Math.min(Math.max(values[0], -100), config().getInt("AttackSpeed.UpperLimit", Integer.MAX_VALUE));
    }

    @Override
    public double calculationCombatPower(double[] values) {
        return values[0] * config().getInt("AttackSpeed.CombatPower");
    }

    @EventHandler
    void onSXItemSpawnEvent(SXItemSpawnEvent e){
        if (e.getItem() != null && !e.getItem().getType().equals(Material.AIR)){
            SXAttributeData attributeData = SXAttribute.getApi().loadItemData(e.getPlayer(), e.getItem());
            IGenerator ig = e.getIg();
            if (ig != null){
                if (config().getStringList("SupportIGList").contains(ig.getType())) {
                    double attackSpeed = getAttackSpeed(e.getItem());
                    double aDouble = attributeData.getValues(getName())[1];
                    double speed =  aDouble <= 0 ? attackSpeed : aDouble;
                    if (speed > -1) {
                        ItemStack itemStack = NMSBase.updateAttack(e.getItem(), speed);
                        e.setItem(itemStack);
                    }
                }
            }
        }


    }



    /**
     * 获取物品攻击速度
     *
     * @param item ItemStack
     * @return double
     */
    public double getAttackSpeed(ItemStack item) {
        return Math.min(config().getDouble("ItemDefaultSpeed." + item.getType().name(), -1), 4);
    }


}

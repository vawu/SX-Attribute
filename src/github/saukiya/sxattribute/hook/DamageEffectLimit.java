package github.saukiya.sxattribute.hook;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.EnumWrappers;
import github.saukiya.sxattribute.SXAttribute;
import me.clip.placeholderapi.expansion.PlaceholderExpansion;

import java.util.List;

public class DamageEffectLimit {

    public DamageEffectLimit() {
        ProtocolManager pm = ProtocolLibrary.getProtocolManager();
        pm.addPacketListener(new PacketAdapter(SXAttribute.getInst(), ListenerPriority.NORMAL, PacketType.Play.Server.WORLD_PARTICLES) {
            public void onPacketSending(PacketEvent event) {
                final PacketContainer pc = event.getPacket();
                final List<EnumWrappers.Particle> particles = pc.getParticles().getValues();
                if (particles.contains(EnumWrappers.Particle.DAMAGE_INDICATOR)) {
                    int max = 10;
                    int min = 1;
                    int index = particles.indexOf(EnumWrappers.Particle.DAMAGE_INDICATOR);
                    final int rand = SXAttribute.getRandom().nextInt(max - min + 1) + min;
                    event.getPacket().getIntegers().write(index, rand);
                }
            }
        });
    }
}

package github.saukiya.sxattribute.util.nms;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public interface BukkitVersion {

    /**
     * 发送title
     * @param player player
     * @param title title
     * @param subTitle subTitle
     * @param a a
     * @param b b
     * @param c c
     */
    void sendTitle(Player player, String title,String subTitle,int a,int b,int c);

    /**
     * 是否是无法破坏
     * @param itemMeta itemMeta
     * @return is
     */
    boolean isUnbreakable(Object itemMeta);

    ItemStack setUnbreakable(ItemStack itemStack,boolean unbreakable);

    /**
     * 主手武器
     * @param player player
     * @return item
     */
    ItemStack getMainItem(Player player);

    /**
     * 设置耐久
     * @param itemStack item
     */
    ItemStack setDurability(Object itemStack,int value);


    int getDurability(Object itemStack);

    void sendActionBar(Player player,String msg);

}

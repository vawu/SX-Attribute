package github.saukiya.sxattribute.util.jsutil;

import github.saukiya.sxattribute.SXAttribute;

import java.util.logging.Level;

public class JsLogger {

    /**
     * 向服务端发送信息
     * info('{0}是最{1}的','我','棒')
     * @param info 信息
     * @param objects 变量
     */
    public void info(String info,Object... objects){
        SXAttribute.getInst().getLogger().log(Level.INFO,info,objects);
    }

}

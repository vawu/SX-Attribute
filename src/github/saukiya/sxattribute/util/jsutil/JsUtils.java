package github.saukiya.sxattribute.util.jsutil;

import github.saukiya.sxattribute.util.Message;
import lombok.Getter;

public class JsUtils {

    public static JsStringUtils stringUtils = new JsStringUtils();

    public static JsLogger logger = new JsLogger();

    public static Message.Tool tool = Message.getTool();

    public static Message.Tool getTool() {
        return tool;
    }

    public static JsStringUtils getStringUtils() {
        return stringUtils;
    }

    public static JsLogger getLogger() {
        return logger;
    }
}

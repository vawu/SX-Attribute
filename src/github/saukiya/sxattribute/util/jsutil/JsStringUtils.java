package github.saukiya.sxattribute.util.jsutil;

public class JsStringUtils {

    /**
     * 字符串是否以value开头
     * @param string string
     * @param value prefix
     * @return boolean
     */
    public boolean startsWith(String string,String value){
        return string.startsWith(value);
    }

    /**
     * 获取属性值
     * @param lore lore
     * @return number
     */
    public double getNumber(String lore) {
        String str = lore.replaceAll("\u00a7+[a-z0-9]", "").replaceAll("[^-0-9.]", "");
        return str.length() == 0 || str.replaceAll("[^.]", "").length() > 1 ? 0D : Double.parseDouble(str);
    }

}

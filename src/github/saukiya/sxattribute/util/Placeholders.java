package github.saukiya.sxattribute.util;

import com.sun.deploy.net.proxy.WFirefoxProxyConfig;
import github.saukiya.sxattribute.SXAttribute;
import github.saukiya.sxattribute.data.attribute.SXAttributeData;
import github.saukiya.sxattribute.data.attribute.SubAttribute;
import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import me.clip.placeholderapi.external.EZPlaceholderHook;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Placeholders {

    static Map<UUID, SXAttributeData> dataMap = new HashMap<>();


    public static void setup() {
        Bukkit.getScheduler().runTaskTimer(SXAttribute.getInst(), ()-> dataMap.clear(), 15, 15);
        new Hook().register();
    }

    public static String onPlaceholderRequest(Player player, String string, SXAttributeData attributeData) {
        if (string.equals("Money") && SXAttribute.isVault())
            return SXAttribute.getDf().format(MoneyUtil.get(player));
        if (string.equals("CombatPower")) return SXAttribute.getDf().format(attributeData.getCombatPower());
        for (SubAttribute attribute : SubAttribute.getAttributes()) {
            Object obj = attribute.getPlaceholder(attributeData.getValues(attribute), player, string);
            if (obj != null) {
                return obj instanceof Double ? SXAttribute.getDf().format(obj) : obj.toString();
            }
        }
        return "§cN/A - " + string;
    }

    public static class Hook extends PlaceholderExpansion {

        @Override
        public String getIdentifier() {
            return "sx";
        }

        @Override
        public String getAuthor() {
            return SXAttribute.getInst().getDescription().getAuthors().toString();
        }

        @Override
        public String getVersion() {
            return SXAttribute.getInst().getDescription().getVersion();
        }

        @Override
        public String onPlaceholderRequest(Player player, String string) {
            SXAttributeData data = dataMap.get(player.getUniqueId());
            if (data == null) {
                data = SXAttribute.getAttributeManager().getEntityData(player);
                dataMap.put(player.getUniqueId(), data);
            }
            return Placeholders.onPlaceholderRequest(player, string, data);
        }
    }
}

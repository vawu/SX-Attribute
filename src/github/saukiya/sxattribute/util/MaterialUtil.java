package github.saukiya.sxattribute.util;

import github.saukiya.sxattribute.SXAttribute;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.logging.Level;

public class MaterialUtil {




    public static ItemStack fromString(String string,String... def){
        String[] split = string.split(":");
        return fromString(split[0],(split.length == 2 ? Short.valueOf(split[1]) : 0) , def);
    }

    public static ItemStack fromString(String string,short data,String... def){
        ItemStack itemStack = null;
        try {
            itemStack = toItemStack(string.toUpperCase());
        }catch (Exception e){
            try {
                for (String s : def) {
                    itemStack = toItemStack(s.toUpperCase());
                    break;
                }
            }catch (Exception e1){}
        }
        if (data != 0 && itemStack != null){
            itemStack.setDurability(data);
        }
        if (itemStack == null) {
            SXAttribute.getInst().getLogger().log(Level.WARNING,"未找到ID为 "+string+" 的物品");
        }
        return itemStack;
    }

    public static Material fromMaterial(String string,String... def){
        Material material = null;
        try {
            material = Material.getMaterial(string.toUpperCase());
        }catch (Exception e){
            try {
                for (String s : def) {
                    material = Material.getMaterial(s.toUpperCase());
                    break;
                }
            }catch (Exception e1){}
        }
        return material;
    }


    public static ItemStack toItemStack(String material){
        Material material1 = Material.valueOf(material.toUpperCase());
        return toItemStack(material1);
    }
    public static ItemStack toItemStack(Material material){
        return new ItemStack(material);
    }

}

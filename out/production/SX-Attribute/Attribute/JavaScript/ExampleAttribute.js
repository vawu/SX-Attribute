/**
 * @valuesLength 属性值长度,默认0
 * @types 类型
 *   ATTACK 攻击
 *   DEFENCE 防御
 *   UPDATE 更新
 *   OTHER 其他
 * @API
 *   Bukkit Bukkit类
 *   API SXApi类
 *   Arrays Arrays 集合类
 *   SXAttributeType 声明属性类型(types)
 *   SXAttribute SX主类
 *   util SX提供的Js工具类
 *
 */
let valuesLength = 1;
let types = Arrays.asList(SXAttributeType.ATTACK);

/**
 * 计算战力值
 * @param values
 */
function calculationCombatPower(values) {
    return values[0];
}
/**
 * 纠正属性值
 * @values
 */
function correct(values) {

}
/**
 * 读取属性
 * @param values
 * @param lore
 */
function loadAttribute(values,lore) {
    if (lore.contains('example')){
        values[0] += util.stringUtils.getNumber(lore)
    }
}

/**
 * 返回显示的数据
 * @param values 数组
 * @param player 玩家
 * @param string key
 * @returns {*}
 */
function getPlaceholder(values,player,string) {
    return string === getPlaceholders() ? values[0] : 'N/O';
}

/**
 * 声明PlaceholderAPI所显示的key
 * 格式为%sx_<name>%
 * @returns {string[]}
 */
function getPlaceholders() {
    return ['example_attribute'];
}

/**
 * 事件处理方法 EventData
 * DamageData 伤害
 * UpdateData 刷新
 * event.getClass().getSimpleName 获取事件名字
 */
function eventMethod(values,event) {
    if (event.getClass.geSimpleName === "DamageData"){
        //获取玩家
        var player = event.getEntity();
        player.sendMessage('你攻击了')
    }
}
/**
 * 默认生成的文件配置
 */
function defaultConfig(config) {
    config.set('message','我是JsExample属性');
    return config;
}

/**
 * 启动
 */
function onEnable() {
    util.logger.info('{0} Js拓展属性启动成功!','Example_Attribute')
}

/**
 * 重载
 */
function onReLoad() {
    util.logger.info('{0} Js拓展属性重载了','Example_Attribute')
}

/**
 * 卸载
 */
function onDisable() {
    util.logger.info('{0} Js拓展属性卸载了','Example_Attribute')
}

